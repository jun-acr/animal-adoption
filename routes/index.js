var express = require('express');
var router = express.Router();
var bodyParser= require("body-parser");
var animal = require('../controllers/animal');
var Animal = require('../models/animal');
var form = require('../controllers/forms');
var multer = require('multer');

// storage defines the storage options to be used for file upload with multer
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/');
    },
    filename: function (req, file, cb) {
        var original = file.originalname;
        var file_extension = original.split(".");
        // Make the file name the date + the file extension
        filename =  Date.now() + '.' + file_extension[file_extension.length-1];
        cb(null, filename);
    }
});
var upload = multer({ storage: storage });

/* GET home page. */
router.get('/', function(req, res, next) {
  animal.list(req,res);
});

/* GET home page using localhost:3000/index. */
router.get('/index', function(req, res, next) {
    animal.list(req,res);
});

router.get('/search', function(req, res, next) {
    res.render('search', { title: 'Search' });
});

router.post('/search',function(req, res) {
    //console.log(req);
    animal.search(req,res);
});

/* GET adoption page. */
router.get('/adoption', function(req, res, next) {
    res.render('adoption', { title: 'Apply to adopt an animal' });
});

router.post('/adoption', function(req, res) {
    console.log(req);
    form.create(req,res);
});

router.get('/add', function(req, res, next) {
    res.render('add', { title: 'Add new animals' });
});

router.post('/add', upload.single('myImg'), function(req, res) {
    console.log(req);
    animal.create(req,res);
});

router.get('/display',function(req,res){

    var id = req.query.id;
    Animal.find({_id: id}, '_id animal_type address photo comments', function (err, animals) {
        if (err) {
            return res.send(500, err);
        }
        res.render('display', {
            data: animals

        });
    });
})

router.post('/display',function(req, res) {
    //console.log(req);
    animal.comment(req,res);
});

router.get('/thanks', function(req, res, next) {
    res.render('thanks', { title: 'Thank you!' });
});

module.exports = router;

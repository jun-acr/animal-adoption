var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var AnimalSchema = new Schema(
    {
        address: {type: String, required: true, max: 100},
        animal_type: {type: String, required: true, max: 100},
        photo: {type: String },
        id: {type: String },
        comments: {type: Array},
        adopted: {type: Boolean}
    }
);


AnimalSchema.set('toObject', {getters: true});

//On some combionations of Node and Mongoose only the following command works - in theory they should be equivalent
//CharacterSchema.set('toObject', {getters: true, virtuals: true});

// the schema is useless so far
// we need to create a model using it
var Adoption = mongoose.model('animals', AnimalSchema);

// make this available to our users in our Node applications
module.exports = Adoption;
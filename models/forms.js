var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var FormSchema = new Schema(
    {
        firstname: {type: String, required: true, max: 100},
        lastname: {type: String, required: true, max: 100},
        mobile: {type: String, required: true, max: 100},
        animal_id: {type: String, required: true, max: 100}
    }
);


FormSchema.set('toObject', {getters: true});

//On some combionations of Node and Mongoose only the following command works - in theory they should be equivalent
//CharacterSchema.set('toObject', {getters: true, virtuals: true});

// the schema is useless so far
// we need to create a model using it
var Adoption = mongoose.model('forms', FormSchema);

// make this available to our users in our Node applications
module.exports = Adoption;
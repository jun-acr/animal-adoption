function sendAjaxQuery(url, data) {
    //console.log(data);
    $.ajax({
        url: url ,
        data: data,
        dataType: 'json',
        type: 'POST',
        success: function (dataR) {
            // no need to JSON parse the result, as we are using
            // dataType:json, so JQuery knows it and unpacks the
            // object for us before returning it
            var ret = dataR;

            // in order to have the object printed by alert
            // we need to JSON stringify the object
            var htmlStr = "";

            for(let v of ret){
                if(!(v.adopted)){
                    htmlStr+=`
                    <p class="animal">
                        <span><img width="100" src="/uploads/${v.photo}"></span></br>
                        <span>_id： ${v._id}</span></br>
                        <span>animal_type： ${v.animal_type}</span></br>
                        <span>address： ${v.address}</span></br>
                        <a href="/display?id=${v._id}">More Info</a></br>
                    </p>
                `
                }else {
                    htmlStr+=`
                    <p class="animal">
                        <span><img width="100" src="/uploads/${v.photo}"></span></br>
                        <span>animal_type： ${v.animal_type}</span></br>
                        <span>address： ${v.address}</span></br>
                        <span>Adopted</span></br>
                    </p>
                `
                }

            }
            document.getElementById('results').innerHTML= htmlStr || "No data";
      
        },
        error: function (xhr, status, error) {

            alert('Error: ' + error.message);
        }
    });
}

function onSubmit(url) {
    var formArray= $("form").serializeArray();
    var data={};
    for (index in formArray){
        data[formArray[index].name]= formArray[index].value;
    }
    console.log(data);
    // const data = JSON.stringify($(this).serializeArray());
    sendAjaxQuery(url,data);
    event.preventDefault();
}
var bodyParser = require("body-parser");
var req = require('request');
var Animal = require('../models/animal');
var path = require('path');


exports.create = function (req, res) {

    var animalData = req.body;
    var arr = new Array();
    var animal = new Animal({
        address: animalData.address,
        animal_type: animalData.animal_type,
        photo: req.file.path.replace("public\\uploads\\",""),
        comments: arr,
        adopted: false
    });

    animal.save(function (err, results) {
        if (err)
            res.status(500).send('Invalid data!');
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(animal));
    });
};

exports.list = function (req, res) {
    Animal.find({}, '_id address animal_type photo adopted', function (err, animals) {
        if (err) {
            return res.send(500, err);
        }
        res.render('index', {
            title: "Here all all the available animals",
            data: animals
        });
    });
};


exports.comment = function (req, res) {
    var animalData = req.body;
    console.log(animalData);
    Animal.findOne({_id: animalData.id}, '_id animal_type address photo comments', function (err, animalFound) {
        if (err) {
            return res.send(500, err);
        }
        else{
            Animal.updateOne({_id: animalData.id},{$push:{comments:animalData.comments}}, function (err, animalUpdated) {
                if (err) {
                    return res.send(500, err);
                }
                else{
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(animalFound));
                }
            });
        }
    });
};

exports.search = function (req, res) {
    var animalData = req.body;
    console.log(animalData);
    switch (animalData.sel) {
        case "type":
            Animal.find({animal_type: animalData.input}, '_id animal_type address photo adopted', function (err, animals) {
                if (err) {
                    return res.send(500, err);
                }
                else{
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(animals));
                }
            });
            break;
        case "address":
            Animal.find({address: animalData.input}, '_id animal_type address photo adopted', function (err, animals) {
                if (err) {
                    return res.send(500, err);
                }
                else{
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(animals));
                }
            });
            break;
        case "id":
            Animal.find({_id: animalData.input}, '_id animal_type address photo adopted', function (err, animals) {
                if (err) {
                    return res.send(500, err);
                }
                else{
                    res.setHeader('Content-Type', 'application/json');
                    res.send(JSON.stringify(animals));
                }
            });
    }

};

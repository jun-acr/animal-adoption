var bodyParser = require("body-parser");
var req = require('request');
var Form = require('../models/forms');
var Animal = require('../models/animal');
var path = require('path');


exports.create = function (req, res) {

    var formData = req.body;
    var form = new Form({
        firstname: formData.firstname,
        lastname: formData.lastname,
        mobile:formData.mobile,
        animal_id: formData.animal_id
    });



    Animal.findOne({_id: formData.animal_id}, '_id animal_type address photo comments adopted', function (err, animalFound) {
        if (err) {
            return res.send(500, err);
        }
        else{

            Animal.updateOne({_id:formData.animal_id},{$set:{adopted:true}}, function (err, animalUpdated) {
                if (err) {
                    return res.send(500, err);
                }
                else{
                    form.save(function (err, results) {
                        if (err)
                            res.status(500).send('Invalid data!');
                        res.setHeader('Content-Type', 'application/json');
                        res.send(JSON.stringify(form));
                    });
                }
            });
        }
    });

};